import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AcademyComponent} from "./layout/components/academy/academy.component";
import {CareerComponent} from "./layout/components/career/career.component";
import {ClientComponent} from "./layout/components/client/client.component";
import {SupplierComponent} from "./layout/components/supplier/supplier.component";
import {NotFoundComponent} from "./layout/components/not-found/not-found.component";

const routes: Routes = [
  { path: '', component:ClientComponent },
  { path: 'client', component: ClientComponent},
  { path: 'academy', component: AcademyComponent},
  { path: 'career', component: CareerComponent},
  { path: 'supplier', component: SupplierComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
