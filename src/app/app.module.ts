import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/components/header/header.component';
import { MainComponent } from './layout/components/main/main.component';
import { FooterComponent } from './layout/components/footer/footer.component';
import { ClientComponent } from './layout/components/client/client.component';
import { AcademyComponent } from './layout/components/academy/academy.component';
import { SupplierComponent } from './layout/components/supplier/supplier.component';
import { CareerComponent } from './layout/components/career/career.component';
import { NotFoundComponent } from './layout/components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    ClientComponent,
    AcademyComponent,
    SupplierComponent,
    CareerComponent,
    NotFoundComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularSvgIconModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
