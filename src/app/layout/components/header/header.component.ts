import {Component, OnInit} from '@angular/core';
import {FormBuilder,FormGroup} from '@angular/forms';

interface IBrand {
  id: number;
  name: string;
}

interface IModel {
  id: number;
  name: string;
}

interface ICar {
  vin: string;
  brand: number;
  model: number;
  typeSearch: string;
}

export const BRANDS: IBrand[] = [
  {id: 1, name: 'Audi'},
  {id: 2, name: 'BMW'},
  {id: 3, name: 'CITROEN'},
  {id: 4, name: 'FORD'},
  {id: 5, name: 'HONDA'},
  {id: 6, name: 'HYUNDAI'},
  {id: 7, name: 'KIA'},
  {id: 8, name: 'MERCEDES'},
  {id: 9, name: 'RENAULT'}
];

export const MODELS: IModel[] = [
  {id: 1, name: 'Arkana'},
  {id: 2, name: 'Duster'},
  {id: 3, name: 'Kaptur'},
  {id: 4, name: 'Logan'},
  {id: 5, name: 'Logan Stepway'},
  {id: 6, name: 'Sandero'},
  {id: 7, name: 'Sandero Stepway'}
];


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  brands = BRANDS;
  models = MODELS;
  searchCarForm: FormGroup;


  constructor(private fb: FormBuilder) {
    this.searchCarForm = fb.group({
      vin: [''],
      brand: [-1],
      model: [-1],
      typeSearch: ['exactly']
    });
  }

  ngOnInit(): void {
  }

  onSubmit(car :ICar) {
    console.log(car);
  }
  onClickRegistration() {
    console.log('Click registration');
  }

  onClickLogin() {
    console.log('Click login');
  }

  onClickPhone() {
    console.log('Click phone');
  }

  onClicLanguage() {
    console.log('Click language');
  }


  onClickCatalog() {
    console.log('Click catalog');
  }

  onClickHelpSelection() {
    console.log('Click help in the selection');
  }

  onClickDownload() {
    console.log('Click download');
  }

  onClickBasket() {
    console.log('Click basket');
  }


  onClickParamOrder() {
    console.log('Click param order');
  }
}
